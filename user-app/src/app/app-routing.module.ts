import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookComponent } from './pages/book/book.component';
import { LoginComponent } from './pages/login/login.component';
import { ServicesComponent } from './pages/services-compoent/services.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from './services/auth.guard'
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  { path: 'auth', component: LoginComponent },
  { path: 'home', canActivate: [AuthGuard], component: ServicesComponent },
  { path: 'welcome', canActivate: [AuthGuard], component: WelcomeComponent },
  { path: 'profile', canActivate: [AuthGuard], component: ProfileComponent },
  { path: 'book/:name', canActivate: [AuthGuard], component: BookComponent },
  { path: '**', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
