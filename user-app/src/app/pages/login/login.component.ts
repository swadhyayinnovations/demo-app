import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './../../services/user';
import { CommonService } from 'src/app/services/common.service';
import * as Parse from 'parse'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: { username: "", email: "", password: "", name: "", number: "" };
  signuptoggle = false;
  constructor(private customerService: User, private router: Router, private commonService: CommonService) { }

  ngOnInit(): void {
    this.user = { username: "", email: "", password: "", name: "", number: "" };

    if (this.customerService.getUsername()) {
      this.router.navigateByUrl("/home")
    }
  }
  signin() {
    this.commonService.spinner = true;
    this.customerService.signIn(this.user).then(res => {
      if (res) {
        this.commonService.user = this.user.username;

        if (this.signuptoggle) {
          this.router.navigateByUrl('/welcome')
        } else {
          this.router.navigateByUrl('/home')
        }


      } else {
        alert("Wrong Credentials")
      }
    }).finally(() => {
      this.commonService.spinner = false;
    })
  }
  signup() {
    this.commonService.spinner = true;
    console.log(this.user)
    Parse.Cloud.run("RegisterCustomer", this.user).then(res => {
      if (res) {
        this.signin()
      } else {
        this.commonService.showToast("Something went wrong")
      }
    }).finally(() => {
      this.commonService.spinner = false;
    })
  }
}
