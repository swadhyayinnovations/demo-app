import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { Services } from 'src/app/services/services.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
services : Services[]
  constructor(public servicesService : Services , public commonService : CommonService) { }

  ngOnInit(): void {
    this.getAll()
  }

  getAll(){
    this.servicesService.load().then(res=>{
      this.services = res;
    })
  }
}
