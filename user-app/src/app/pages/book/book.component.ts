import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Bookings } from 'src/app/services/bookings.service'
import { CommonService } from 'src/app/services/common.service';
import { Services } from 'src/app/services/services.service';
@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {
  name;
  service: Services

  constructor(public servicesService: Services, private activatedRoute: ActivatedRoute, public router: Router, public commonService: CommonService) { }

  ngOnInit(): void {

    this.activatedRoute.params.subscribe(async res => {
      this.commonService.spinner = true
      this.name = res.name
      this.service = await this.servicesService.getService(this.name).finally(() => {
        this.commonService.spinner = false;
      })

    })
  }

}
