import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { User } from '../services/user';
import * as Parse from 'parse'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(public userService: User, public common: CommonService) { }
  user: User;
  ngOnInit(): void {
    this.user = User.getCurrent();
  }
  save() {
    this.common.spinner = true;
    Parse.Cloud.run('updateCustomer', this.user.attributes).then(res => {
      this.common.showToast("Saved Successfully")
      this.common.spinner = false;

    }).catch(err => {
      this.common.showToast("Error")
      this.common.spinner = false;
    })
  }
}
