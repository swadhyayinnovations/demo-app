import { Component } from '@angular/core';
import { Router } from '@angular/router';
import *  as Parse from 'parse';
import { environment } from 'src/environments/environment';
import { threadId } from 'worker_threads';
import { Bookings } from './services/bookings.service';
import { CommonService } from './services/common.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private router: Router, public bookingService: Bookings, public commonService: CommonService) {
    this.setupParse()


  }
  setupParse() {
    (Parse as any).serverURL = environment.serverUrl;
    Parse.initialize(environment.appId);
    this.checkLogin()
  }
  checkLogin() {
    this.commonService.user = Parse.User.current()
  }
  logout() {
    Parse.User.logOut()
    window.location.reload()
  }


  GetMyBookings() {
    this.bookingService.GetMyBookings().then(e => {
      this.bookingService.subscription.on('update', (booking: any) => {

        if (booking.number != 0 && booking.status == "Pending") {
          this.commonService.showToast('Your booking at ' + booking.service + ' has been updated and now you can checkin in ' + booking.number * 5 + " minutes.", { classname: 'bg-success text-light', delay: 10000 })

        } else if (booking.status == "Pending") {
          this.commonService.showToast('Your booking at ' + booking.service + ' has been updated and now you can checkin in now', { classname: 'bg-success text-light', delay: 10000 })
        }


      });


    })
  }
}
