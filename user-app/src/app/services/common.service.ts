import { Injectable, TemplateRef } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
user;
toast;
contact;
spinner = false;
  constructor() { }

  showToast(textOrTpl: string | TemplateRef<any>, options: any = {}){
this.toast = {textOrTpl,options}
       setTimeout(() => {
  this.toast = null
}, 5000);
  }
}
