import { Injectable } from '@angular/core';
import * as Parse from 'parse';
@Injectable({
  providedIn: 'root'
})
export class Services extends Parse.Object {

  constructor() {
    super('Services');
  }

  static getInstance() {
    return this;
  }


  load(): Promise<Services[]> {

    const query = new Parse.Query(Services);

    query.ascending('name');
    query.doesNotExist('deletedAt');

    return query.find()
  }
  getService(name) {

    const query = new Parse.Query(Services);
    query.equalTo("name", name)
    query.ascending('name');
    query.doesNotExist('deletedAt');

    return query.first()
  }

  get name(): string {
    return this.get('name');
  }

  get type(): string {
    return this.get('type');
  }


  get description(): string {
    return this.get('description');
  }







}

Parse.Object.registerSubclass('Services', Services);
