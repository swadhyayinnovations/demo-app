import { Injectable } from '@angular/core';
import * as Parse from 'parse';
@Injectable({
  providedIn: 'root'
})
export class Bookings extends Parse.Object {
  subscription: Parse.LiveQuerySubscription;
  bookings
  count
  countSubcription
  constructor() {
      super('Bookings');
  }

  static getInstance() {
      return this;
  }

  
  Create(booking): Promise<Bookings> {
    const obj = new Bookings();
    return obj.save(booking);
  }

  async GetMyBookings(){
    const query = new Parse.Query(Bookings);
    query.equalTo('customer',localStorage.getItem("name") );
    query.descending('createdAt');
    this.subscription = await query.subscribe();
    this.bookings =await query.find()
  }

  
 async getBookingNumber(name){
    const query = new Parse.Query(Bookings);
    
    query.equalTo('service',name );
    query.doesNotExist('deletedAt');
    query.doesNotExist('alloted');
    this.countSubcription = await query.subscribe();
    this.count =  await   query.count();
    this.countSubcription.on('create', (booking : any) => {
      console.log("hello")
this.getBookingNumber(name)
      })
  }
  get service(): string {
      return this.get('service');
  }
 

  get status(): string {
    return this.get('status');
}
get number(): string {
  return this.get('number');
}

  get customer(): string {
      return this.get('customer');
  }


 


  
}

Parse.Object.registerSubclass('Bookings', Bookings);
