import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { User } from 'src/app/services/user';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user= { username: "", email: "", password: "", name: "", number: "" }
  constructor(private userService: User, private router: Router, private commonService: CommonService) { }

  ngOnInit(): void {

  }
  signin() {
    this.commonService.spinner = true;
    this.userService.signIn(this.user).then(res => {
      this.router.navigateByUrl('/home');
      this.commonService.spinner = false;
    }).finally(() => {
      this.commonService.spinner = false;
    })
  }

}
