import { Component, OnInit } from '@angular/core';
import { User } from '../../services/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  users: User[]
  constructor(public userService: User) { }

  ngOnInit(): void {
    this.userService.getAll().then((res: any) => {
      this.users = res;
      console.log(res)
    })
  }

}
