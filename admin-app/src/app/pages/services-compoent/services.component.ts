import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { Services } from 'src/app/services/services.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
  services: Services[]
  add;
  newservice: Services;
  constructor(public servicesService: Services, public commonService: CommonService) { }

  ngOnInit(): void {
    this.getAll()
  }

  addService() {
    this.add = true;
    this.newservice = new Services()
  }
  getAll() {
    this.servicesService.load().then(res => {
      this.services = res;
    })
  }
  save() {
    this.commonService.spinner = true;
    this.newservice.save().then(res => {
      this.commonService.showToast("Saved Successfully")
      this.commonService.spinner = false;
      this.getAll()
      this.add = false;
    }).catch(err => {
      this.commonService.showToast("Error")
      this.commonService.spinner = false;
    })
  }
}
