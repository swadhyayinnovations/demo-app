import { Injectable } from '@angular/core';
import * as Parse from 'parse';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class Test extends Parse.Object {
   async loadFeaturedNavBar() {

        const query = new Parse.Query(Test);
        let subscription = await query.subscribe();
        
        subscription.on('enter', (people) => {
            console.log(people); // This should output Mengyan
            return people
          });
          subscription.on('update', (people) => {
            console.log(people); // This should output 100
            return people
          });
    }
}

Parse.Object.registerSubclass('Test', Test);