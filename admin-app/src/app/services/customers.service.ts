import { Injectable } from '@angular/core';
import * as Parse from 'parse';
@Injectable({
  providedIn: 'root'
})
export class Customers extends Parse.Object {

  constructor() {
      super('Customers');
  }

  static getInstance() {
      return this;
  }

  login(user): Promise<Customers> {

      const query = new Parse.Query(Customers);
      query.equalTo('name',user.name );
      query.equalTo('contact', user.contact);
      query.doesNotExist('deletedAt');

      return query.first()
  }
 
  
  signUp(user): Promise<Customers> {
    const obj = new Customers();
    return obj.save(user);
  }
  
  get name(): string {
      return this.get('name');
  }
  set name(val: string) {
    this.set('name', val);
  }


  get contact(): string {
      return this.get('contact');
  }


  set contact(val: string) {
    this.set('contact', val);
  }


  
}

Parse.Object.registerSubclass('Customers', Customers);
