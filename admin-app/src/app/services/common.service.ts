import { Injectable, TemplateRef } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  service
  toast;
  spinner = false;
    constructor() { }
  
    showToast(textOrTpl: string | TemplateRef<any>, options: any = {}){
  this.toast = {textOrTpl,options}
         setTimeout(() => {
    this.toast = null
  }, 20000);
    }
}
