import { Injectable } from '@angular/core';
import * as Parse from 'parse';
@Injectable({
  providedIn: 'root'
})
export class Services extends Parse.Object {

  constructor() {
    super('Services');
  }

  static getInstance() {
    return this;
  }

  login(service): Promise<Services> {

    const query = new Parse.Query(Services);
    query.equalTo('name', service);

    query.doesNotExist('deletedAt');

    return query.first()
  }
  load(): Promise<Services[]> {

    const query = new Parse.Query(Services);

    query.ascending('name');
    query.doesNotExist('deletedAt');

    return query.find()
  }

  get name(): string {
    return this.get('name');
  }

  set name(val) {
    this.set('name', val);
  }
  get type(): string {
    return this.get('type');
  }
  set type(val) {
    this.set('type', val);
  }

  get description(): string {
    return this.get('description');
  }
  set description(val) {
    this.set('description', val);
  }
}





Parse.Object.registerSubclass('Services', Services);
