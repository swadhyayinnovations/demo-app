import { Injectable } from '@angular/core';
import * as Parse from 'parse';
@Injectable({
  providedIn: 'root'
})
export class Bookings extends Parse.Object {
subscription
  constructor() {
      super('Bookings');
  }

  static getInstance() {
      return this;
  }

  
  Create(booking): Promise<Bookings> {
    const obj = new Bookings();
    return obj.save(booking);
  }

async  GetMyBookings(){
    const query = new Parse.Query(Bookings);
    query.equalTo('service',localStorage.getItem("service") );
    query.descending('createdAt');
    this.subscription = await query.subscribe();

    
    return   query.find()

  
   

  }
  
  getBookingNumber(name){
    const query = new Parse.Query(Bookings);
    query.equalTo('service',name );
    query.doesNotExist('deletedAt');
    query.doesNotExist('alloted');
  return   query.count()
  }

  CheckinUser(id): Promise<Bookings> {
 
    return Parse.Cloud.run('CheckinCustomer', { id });
  }
  get service(): string {
      return this.get('service');
  }
 

  get status(): string {
    return this.get('status');
}
get number(): string {
  return this.get('number');
}

  get customer(): string {
      return this.get('customer');
  }


 


  
}

Parse.Object.registerSubclass('Bookings', Bookings);
