import { Component } from '@angular/core';
import { Router } from '@angular/router';
import *  as Parse from 'parse';
import { environment } from 'src/environments/environment';
import { CommonService } from './services/common.service'
import { User } from './services/user';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private router: Router, public user: User, public commonService: CommonService) {
    this.setupParse()

  }
  setupParse() {
    (Parse as any).serverURL = environment.serverUrl;
    Parse.initialize(environment.appId);

  }

  logout() {
    User.logOut()
    window.location.reload()
  }



}
