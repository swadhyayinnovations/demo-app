import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { ServicesComponent } from './pages/services-compoent/services.component';
import { AuthGuard } from './services/auth.guard';
import { UserComponent } from './pages/user/user.component';

const routes: Routes = [
  { path: 'auth', component: LoginComponent },
  { path: 'home', canActivate: [AuthGuard], component: ServicesComponent },
  { path: 'user', canActivate: [AuthGuard], component: UserComponent },
  { path: '**', component: ServicesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
