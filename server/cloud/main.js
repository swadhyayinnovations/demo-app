// It is best practise to organize your cloud functions group into their own file. You can then import them in your main.js.
require('./functions.js');


Parse.Cloud.define('RegisterCustomer', async function (req, res) {
    const params = req.params

    const user1 = new Parse.User()
    user1.set('name', params.name)


    user1.set('username', params.username)


    user1.set('email', params.email)

    user1.set('useremail', params.email)
    user1.set('number', params.number)

    user1.set('password', params.password)



    const acl = new Parse.ACL()
    acl.setPublicReadAccess(true)
    acl.setPublicWriteAccess(false)
    user1.setACL(acl)

    await user1.signUp()


    return user1
})

Parse.Cloud.define('updateCustomer', async function (req, res) {
    const params = req.params
    const query = new Parse.Query(Parse.User)
    query.equalTo('username', params.username.trim())


    const user1 = await query.first({ useMasterKey: true })

    if (params.name) {
        user1.set('name', params.name)
    }


    user1.set('email', params.email)

    user1.set('number', params.number)

    user1.set('password', params.password)
    user1.save(null,{useMasterKey:true})

})