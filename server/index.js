// Example express application adding the parse-server module to expose Parse
// compatible API routes.

const express = require('express');
const ParseServer = require('parse-server').ParseServer;
const path = require('path');
const args = process.argv || [];
const test = args.some(arg => arg.includes('jasmine'));
const ParseDashboard = require('parse-dashboard')
const databaseUri = process.env.DATABASE_URI || process.env.MONGODB_URI;

if (!databaseUri) {
  console.log('DATABASE_URI not specified, falling back to localhost.');
}
const config = {
  databaseURI: databaseUri || 'mongodb://localhost:27017/testdb',
  cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
  appId: process.env.APP_ID || 'myAppId',
  masterKey: 'demo', //Add your master key here. Keep it secret!
  serverURL: 'http://127.0.0.1:1337/parse', // Don't forget to change to https if needed
};

// Parse Dashboard
// https://github.com/parse-community/parse-dashboard

// Serve the Parse Dashboard on the /dashboard URL prefix


// Client-keys like the javascript key or the .NET key are not necessary with parse-server
// If you wish you require them, you can set them as options in the initialization above:
// javascriptKey, restAPIKey, dotNetKey, clientKey

const app = express();

// Serve static assets from the /public folder
app.use('/public', express.static(path.join(__dirname, '/public')));
// Serve the Parse API on the /parse URL prefix
const mountPath = process.env.PARSE_MOUNT || '/parse';

const api = new ParseServer(config);
app.use(mountPath, api);




const dashboard = new ParseDashboard({
  apps: [
    {
      serverURL: 'http://127.0.0.1:1337/parse',
      appId: 'myAppId',
      masterKey: 'demo',
      appName: 'POC',
      production: true,
    }
  ],
  users: [

    {
      user: 'admin',
      pass: '$2y$12$PPSj7JKPT/yPbItbGQLhn.nGbAkqCZCGU2eeppubaGZtvLCB9AscS'
    },
  ],
  useEncryptedPasswords: true,
  trustProxy: 1
}, { allowInsecureHTTP: true, cookieSessionSecret: '' });



app.use('/dashboard', dashboard);

// Parse Server plays nicely with the rest of your web routes
app.get('/', function (req, res) {
  res.status(200).send('I dream of being a website.  Please star the parse-server repo on GitHub!');
});

// There will be a test page available on the /test path of your server url
// Remove this before launching your app
app.get('/test', function (req, res) {
  res.sendFile(path.join(__dirname, '/public/test.html'));
});

const port = process.env.PORT || 1337;
if (!test) {
  const httpServer = require('http').createServer(app);
  httpServer.listen(port, function () {
    console.log('parse-server-example running on port ' + port + '.');
  });
  // This will enable the Live Query real-time server
  ParseServer.createLiveQueryServer(httpServer);
}

module.exports = {
  app,
  config,
};
